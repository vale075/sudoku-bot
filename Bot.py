import math
import numpy as np

grid = [
    [4, 0, 5, 0, 6, 0, 0, 0, 7],
    [0, 0, 3, 0, 0, 5, 4, 0, 0],
    [9, 2, 8, 3, 0, 4, 0, 0, 0],
    [1, 5, 0, 4, 0, 0, 8, 0, 3],
    [0, 0, 6, 0, 1, 0, 0, 5, 0],
    [0, 0, 4, 7, 0, 0, 1, 6, 9],
    [6, 0, 0, 5, 0, 1, 9, 4, 8],
    [8, 3, 0, 6, 4, 0, 0, 0, 5],
    [0, 0, 9, 8, 0, 0, 0, 0, 0],
]
grid_solved = [
    [4, 1, 5, 9, 6, 2, 3, 8, 7],
    [7, 6, 3, 1, 8, 5, 4, 9, 2],
    [9, 2, 8, 3, 7, 4, 5, 1, 6],
    [1, 5, 7, 4, 9, 6, 8, 2, 3],
    [3, 9, 6, 2, 1, 8, 7, 5, 4],
    [2, 8, 4, 7, 5, 3, 1, 6, 9],
    [6, 7, 2, 5, 3, 1, 9, 4, 8],
    [8, 3, 1, 6, 4, 9, 2, 7, 5],
    [5, 4, 9, 8, 2, 7, 6, 3, 1],
]

class sudoku():
    def __init__(self, grid) -> None:
        self.grid = grid
        self.size = len(grid)
        self.sizeq = math.isqrt(self.size)

    def cacher(self):
        self.cache = np.full((self.size, self.size, self.size), True)
        for y in range(self.size):
            for x in range(self.size):
                if grid[y][x] != 0:
                    self.cache[
                        y, x, :
                    ] = False  # Cannot place anything on this case (already taken)
                    self.cache[
                        :, x, grid[y][x] - 1
                    ] = False  # Cannot place this specific number on the y axis (column)
                    self.cache[
                        y, :, grid[y][x] - 1
                    ] = False  # Cannot place this specific number on the x axis (ligne)
                    self.cache[
                        y - y % self.sizeq : y - y % self.sizeq + self.sizeq,
                        x - x % self.sizeq : x - x % self.sizeq + self.sizeq,
                        grid[y][x] - 1,
                    ] = False  # Cannot place this specific number in this cell

        print(self.cache)

    def visualizer(self):
        pass

print(np.array(grid))

ngrid = sudoku(grid)
ngrid.cacher()